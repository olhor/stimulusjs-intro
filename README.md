# Stimulus Starter

A preconfigured blank slate for exploring [Stimulus](https://github.com/stimulusjs/stimulus). Jump to [The Stimulus Handbook](https://github.com/stimulusjs/stimulus/blob/master/handbook/README.md) for a quick introduction.

---

We recommend [remixing `stimulus-starter` on Glitch](https://glitch.com/edit/#!/remix/stimulus-starter) so you can work entirely in your browser without installing anything:

[Remix on Glitch](https://glitch.com/edit/#!/remix/stimulus-starter)

Or, if you'd prefer to work from the comfort of your own text editor, you'll need to clone and set up `stimulus-starter`:

```
$ git clone https://github.com/stimulusjs/stimulus-starter.git
$ cd stimulus-starter
$ yarn install
$ yarn start
```

# This pack

This pack already includes examples from the Handbook. To make things work run:

```
$ yarn install
$ yarn start
```
and go to http://localhost:9000

---

� 2018 Basecamp, LLC.
