import { Controller } from "stimulus"

export default class extends Controller {
    static targets = [ "slide" ]

    initialize() {
        const index = parseInt(this.data.get("index")) || 0
        this.showSlide(index)
    }

    next() {
        this.showSlide(this.index + 1)
    }

    previous() {
        this.showSlide(this.index - 1)
    }

    showSlide(index) {
        if (index > this.slideTargets.length - 1) index = 0
        if (index < 0) index = this.slideTargets.length - 1
        this.index = index
        this.slideTargets.forEach((el, i) => {
            el.classList.toggle("slide--current", index == i)
        })
        this.data.set("index", index) // persist index in DOM
    }
}